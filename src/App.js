import './App.scss';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import About from './components/about-us/aboutus';
import Home from './components/home/home';
import Register from './components/Auth/registration/registration';
import Navbar from './components/header/navbar';
import Protected from './components/Routes/Protected';
import { useState, useEffect } from 'react';
import Login from './components/Auth/login/login';
import { initGA4} from './components/analytics';
import ReactGA from 'react-ga4';
import { Sidebar } from 'react-pro-sidebar';

const  App=()=> {
  const MEASUREMENT_ID = "G-8VWP4DNDS4"; // OUR_MEASUREMENT_ID
   //Google Analytics
   useEffect(() => {
    initGA4(MEASUREMENT_ID);
    console.log(ReactGA); 
  }, []);


  const [isSignedIn, setIsSignedIn] = useState(null);
  const signIn = () => {
    setIsSignedIn(true);
  }
  const signOut = () => {
    setIsSignedIn(false);
  }

  return (
    <div className="App">
     <BrowserRouter>
     <Navbar/>
     <Sidebar/>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/about-us" element={<Protected isSignedIn={isSignedIn}> <About /> </Protected>} />
        <Route path="/registration" element={<Register />} />
        <Route path="/login" element={<Login />} />
      </Routes>
      {/* <Footer /> */}
    </BrowserRouter>
    </div>
  );
}

export default App;
