import React from 'react';
import './footer.scss';
const Footer = () => {
    return (
        <>
            <div className='footer-section'>
                <div className='top-section'>

                </div>
                <div className='middle-section'>

                </div>
                <div className='bottom-section'>
                    <div className='version'>All right reserved - Design & Developed by Apptware LLP.</div>
                </div>
            </div>
        </>
    );
}

export default Footer;