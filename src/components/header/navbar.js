import './navbar.scss';
import { Link } from 'react-router-dom';
import React from 'react';
import ReactGA from 'react-ga4';

const Navbar=(userData)=> {

  const handleClick=() =>{
    // Log a custom event with category and action
    ReactGA.event({
      category: 'Click Events',
      action: 'View More',
      label: 'Details page', // Optional: add a label

    });
}

  return (
    <>
      <nav>
        <div className='container'>
        <Link  to='/'> Home </Link>
        </div>
        <div className='container'>
            <Link  to='/tutorials'> Tutorials</Link>
        </div>
        <div className='container'>
            <Link  to='/e-courses'> E-Courses</Link>
        </div>
        <div className='container'>
        <Link  to='/videos'> Videos</Link>
        </div>
        <div className='container'>
        <Link  to='/reference'> Reference</Link>
        </div>
        <div className='container'>
        <Link  to='/about-us'> About Us</Link>
        </div>
        <div className='container'>
        <Link  to='/registration'>Register Now</Link>
        </div>
        <div className='container' onClick={() => handleClick()}>
        <Link  to='/login'>Login</Link>
        </div>
      </nav>
      </>
  );
}

export default Navbar;
