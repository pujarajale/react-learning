import React, { useEffect, useState } from 'react';
import './registration.scss';
import UserList from '../../layout/userlist/userlist';

const Register = (props) => {
    const [userInput, setUserInput] = useState('');

    const [data, setData] = useState([]);

    const changeUserData = (e) => {
        setUserInput(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        props.onSubmit(userInput);
    }
    // * Init on Page Load
    useEffect(() => {
        fetchData();
    }, []);

    //call new fetch api 
    const fetchData = () => {
        fetch('https://jsonplaceholder.typicode.com/posts?_limit=10')
            .then(response => response.json())
            .then(json => setData(json))
            .catch(error => console.error(error));
        console.log(data);
    };

    return (
        <>
            <form onSubmit={handleSubmit}>
                <div className='reg-section'>
                    <div className='card'>
                        <div className='label'>
                            <label>Username</label>
                        </div>
                        <div className='user-input'>
                            <input type='text' placeholder='Enter UserName' onChange={changeUserData} value={userInput} />
                        </div>
                        <div className='label'>
                            <label>E-mail</label>
                        </div>
                        <div className='user-input'>
                            <input type='text' placeholder='Enter email' />
                        </div>
                        <div className='label'>
                            <label>Password</label>
                        </div>
                        <div className='user-input'>
                            <input type='text' placeholder='Enter password' />
                        </div>
                        <div className='submit'>
                            <button type='submit'>Submit</button>
                        </div>
                    </div>
                </div>
            </form>
            <div className='user-data'>
                <UserList usersData={data} />
            </div>
        </>
    );
}

export default Register;


// const Home = (userData) => {

    // const getData=(userData)=>{
    //    console.log(userData);
    // }

//   <Register onSubmit={getData} />
// }