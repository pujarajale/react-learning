import './login.scss';
import {sendEvent} from '../../analytics';

import React, { useState } from 'react';

const Login = () => {

  const [formData, setFormData] = useState({
    username: '',
    password: '',
  });

  const [errors, setErrors] = useState({
    username: '',
    password: '',
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData({
      ...formData,
      [name]: value,
    });
    setErrors({
      ...errors,
      [name]: '', // Clear any previous errors when the user starts typing
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    // Basic validation
    let formIsValid = true;
    const newErrors = { ...errors };

    if (formData.username.trim() === '') {
      formIsValid = false;
      newErrors.username = 'Username is required';
    }

    if (formData.password.trim() === '') {
      formIsValid = false;
      newErrors.password = 'Password is required';
    }

    if (formIsValid) {
      // You can perform your login logic here
    console.log('Login successful! Do something with the data:', formData);
    window.alert("user login successfully!");
     // Trigger the 'New Registration' event
     //   sendEvent('User', 'Form', 'Submission');
    sendEvent('User', 'registration', 'New Registration');
    } else {
      // Update the state with the validation errors
      setErrors(newErrors);
    }
  };

  return (
    <div className='reg-section'>
      <h2>Login Form</h2>
      <form onSubmit={handleSubmit}>
       <div className='card'>
       <label htmlFor="username" className='label'>Username:</label>
       <div className='user-input'>
          <input
            type="text"
            id="username"
            name="username"
            value={formData.username}
            onChange={handleInputChange}
          />
          <span style={{ color: 'red' }}>{errors.username}</span>
        </div>
        <label htmlFor="password" className='label'>Password:</label>
        <div className='user-input'>
          <input
            type="password"
            id="password"
            name="password"
            value={formData.password}
            onChange={handleInputChange}
          />
          <span style={{ color: 'red' }}>{errors.password}</span>
        </div>
         <div className='submit'>
         <button type="submit">Register</button>
         </div>
       </div>
      </form>
    </div>
  );
};

export default Login;

