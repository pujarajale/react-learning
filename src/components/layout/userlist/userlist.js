
import React from 'react';

const UserList=({ usersData })=> {
console.log(usersData);
  return (
    <>
    <div className='user-container'>
      <div><h3>User List Data</h3></div>
      <table colspan='1' border='1'>
      <thead>
      <th>Id</th>
      <th>User Id</th>
      <th>Title</th>
      <th>Description</th>
      </thead>
       
        {usersData && usersData.length > 0 && usersData.map((data) => {
                            return (
                                <>
                                 <tbody>
                                   <td key={data?.id}>
                                    {data?.id}
                                   </td>
                                   <td key={data?.id}>
                                    {data?.userId}
                                   </td>
                                   <td key={data?.id}>
                                   {data?.title}
                                   </td>
                                   <td key={data?.id}>
                                    {data?.body}
                                    </td>
                                   </tbody>
                                </>
                            )
                        })}
     
      </table>
    </div>
      </>
  );
}

export default UserList;
