// analytics.js
import ReactGA from 'react-ga4';

const initGA4=(measurementId)=> {
  ReactGA.initialize(measurementId);
}


const sendEvent = (category, action, label) => {
  const payload = {
    category,
    action,
    label,
  };
  // Log the payload for debugging purposes
  console.log('Sending Event:', payload);
  // Send the event to Google Analytics
  ReactGA.event(payload);
};

const sendPageview = (path) => {
  ReactGA.set({ page: path });
};

export  {
  initGA4,
  sendEvent,
  sendPageview
};
